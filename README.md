<p align="center">
    <img src="https://codeberg.org/PrivacyBlog/Liquidrechner/raw/branch/master/app/src/main/res/drawable/liquid.png" title="Liquidrechner Logo">
</p>

# Liquid Calculator

## Calculator for mixing e-cigarette liquids

<a href="https://f-droid.org/de/packages/org.eukalyptus.liquidrechner/"><img src="https://gitlab.com/fdroid/artwork/-/raw/master/badge/get-it-on-en.png" height="75"></a><br>
<img src="https://img.shields.io/f-droid/v/org.eukalyptus.liquidrechner.svg">
<img src="https://img.shields.io/badge/codeberg-v1.5-blue">
<img src="https://img.shields.io/maintenance/Yes/2021">


This calculator helps you to mix your eLiquids according to your personal preferences:

* Calculate basic recipes
* Calculate Shake'n'Vape recipes
* Mix different liquids and calculate the total nicotine level
* Increase nicotine levels
* Decrease nicotine levels
* Increase the aroma level

## Minimalistic

* No advertising
* No in-app purchases
* No trackers
* Free & Open-Source under GPLv3 license
* Does not require any permissions or internet access
* Built from scratch

## Screenshots

<img src="https://codeberg.org/PrivacyBlog/Liquidrechner/raw/branch/master/fastlane/metadata/android/de-DE/images/phoneScreenshots/1.png" width=350 title="Liquidrechner Logo">    
<img src="https://codeberg.org/PrivacyBlog/Liquidrechner/raw/branch/master/fastlane/metadata/android/de-DE/images/phoneScreenshots/2.png" width=350 title="Liquidrechner Logo">




## Contributing

Feel free to help with this project in any possible way!


## Licenses

### App license:

Free according to [GNU GPLv3 license](https://codeberg.org/PrivacyBlog/Liquidrechner/src/branch/master/LICENSE).

### In-App picture licenses:

* App icon (Bottle icon): Made by [Freepik](https://www.flaticon.com/authors/Freepik) from [Flaticon](www.flaticon.com)
* Ink icon (Base liquid): Made by [Freepik](https://www.flaticon.com/authors/Freepik) from [Flaticon](www.flaticon.com)
* Shaker icon (Shake\'n\'Vape): Made by [Freepik](https://www.flaticon.com/authors/Freepik) from [Flaticon](www.flaticon.com)
* Mixer icon (Mixing liquids): Made by [Freepik](https://www.flaticon.com/authors/Freepik) from [Flaticon](www.flaticon.com)
* Increase-Nicotine-Level-Icon: Made by [Smashicons](https://www.flaticon.com/authors/Smashicons) from [Flaticon](www.flaticon.com)
* Water drop icon (Decrease nicotine level): Made by [Smalllikeart](https://www.flaticon.com/authors/smalllikeart) from [Flaticon](www.flaticon.com)
* Perfume icon (Increase aroma level): Made by [Nhor-Phai](https://www.flaticon.com/authors/Nhor-Phai) from [Flaticon](www.flaticon.com)
