package org.eukalyptus.liquidrechner.ui.aromarechner;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

import com.google.android.material.floatingactionbutton.FloatingActionButton;

import org.eukalyptus.liquidrechner.R;

public class AromarechnerFragment extends Fragment {


    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_aromarechner, container, false);
        FloatingActionButton fab = getActivity().findViewById(R.id.fab);
        fab.show();
        return root;

    }

}