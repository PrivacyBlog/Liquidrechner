package org.eukalyptus.liquidrechner;

import android.app.Activity;
import android.os.Bundle;

import com.google.android.material.floatingactionbutton.FloatingActionButton;

import android.view.MotionEvent;
import android.view.View;

import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.navigation.ui.AppBarConfiguration;
import androidx.navigation.ui.NavigationUI;

import com.google.android.material.navigation.NavigationView;

import androidx.drawerlayout.widget.DrawerLayout;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.view.Menu;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    private AppBarConfiguration mAppBarConfiguration;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        setupUI(findViewById(R.id.drawer_layout));

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        // Floating Action Button
        FloatingActionButton fab = findViewById(R.id.fab);

        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                berechneBasis();
                liquidsMischen();
                verduennen();
                aromarechner();
                nikotinerhoehen();
                shakeandvape();
            }
        });

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        NavigationView navigationView = findViewById(R.id.nav_view);

        // Icons farbig machen
        navigationView.setItemIconTintList(null);

        // Passing each menu ID as a set of Ids because each
        // menu should be considered as top level destinations.
        mAppBarConfiguration = new AppBarConfiguration.Builder(
                R.id.nav_basisrechner, R.id.nav_liquidsmischen, R.id.nav_verduennung, R.id.nav_aromarechner,
                R.id.nav_nikotinerhoehen, R.id.nav_shakeandvape, R.id.nav_about)
                .setDrawerLayout(drawer)
                .build();

        NavController navController = Navigation.findNavController(this, R.id.nav_host_fragment);
        NavigationUI.setupActionBarWithNavController(this, navController, mAppBarConfiguration);
        NavigationUI.setupWithNavController(navigationView, navController);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onSupportNavigateUp() {
        NavController navController = Navigation.findNavController(this, R.id.nav_host_fragment);
        return NavigationUI.navigateUp(navController, mAppBarConfiguration)
                || super.onSupportNavigateUp();
    }

    // Hiding keyboard
    public void hideSoftKeyboard(Activity activity) {
        try {
            InputMethodManager inputMethodManager =
                    (InputMethodManager) activity.getSystemService(
                            Activity.INPUT_METHOD_SERVICE);
            inputMethodManager.hideSoftInputFromWindow(
                    activity.getCurrentFocus().getWindowToken(), 0);
        }catch(Exception e){}

        berechneBasis();
        liquidsMischen();
        verduennen();
        aromarechner();
        nikotinerhoehen();
        shakeandvape();
    }

    public void setupUI(View view) {

        // Set up touch listener for non-text box views to hide keyboard.
        if (!(view instanceof EditText)) {
            view.setOnTouchListener(new View.OnTouchListener() {
                public boolean onTouch(View v, MotionEvent event) {
                    hideSoftKeyboard(MainActivity.this);
                    return false;
                }
            });
        }
        //If a layout container, iterate over children and seed recursion.
        if (view instanceof ViewGroup) {
            for (int i = 0; i < ((ViewGroup) view).getChildCount(); i++) {
                View innerView = ((ViewGroup) view).getChildAt(i);
                setupUI(innerView);
            }
        }
    }



    public void berechneBasis(){
        try {

            // Deklaration
            EditText shotStaerkeInputView = (EditText) findViewById(R.id.basis1StaerkeInput);
            double shotStaerkeInput = Double.parseDouble(shotStaerkeInputView.getText().toString());
            EditText zielmengeInputView = (EditText) findViewById(R.id.basis1MengeInput);
            double zielmengeInput = Double.parseDouble(zielmengeInputView.getText().toString());
            EditText zielstaerkeInputView = (EditText) findViewById(R.id.zielstaerkeInput);
            if(zielstaerkeInputView.getText().toString().equals("")){
                zielstaerkeInputView.setText("0");
            }
            double zielstaerkeInput = Double.parseDouble(zielstaerkeInputView.getText().toString());
            EditText aromaInputView = (EditText) findViewById(R.id.aromaInput);
            if(aromaInputView.getText().toString().equals("")){
                aromaInputView.setText("0");
            }
            double aromaInput = Double.parseDouble(aromaInputView.getText().toString());

            TextView zielmengeShotsView = (TextView) findViewById(R.id.benoetigtShots);
            TextView zielmengeBasisView = (TextView) findViewById(R.id.benoetigtBasis);
            TextView zielmengeAromaView = (TextView) findViewById(R.id.benoetigtAroma);

            // Berechnung

            if(zielstaerkeInput <= shotStaerkeInput && !(zielstaerkeInput == shotStaerkeInput && aromaInput != 0 && shotStaerkeInput != 0 && zielstaerkeInput != 0)){

                double zielmengeShots = zielmengeInput/(shotStaerkeInput/zielstaerkeInput);
                zielmengeShots = (double)Math.round(zielmengeShots * 100d) / 100d;
                double zielmengeAroma = (zielmengeInput*aromaInput/100);
                zielmengeAroma = (double)Math.round(zielmengeAroma * 100d) / 100d;
                double zielmengeBasis = zielmengeInput-zielmengeShots-zielmengeAroma;
                zielmengeBasis = (double)Math.round(zielmengeBasis * 100d) / 100d;


                // Setze Ergebnisse
                zielmengeShotsView.setText(String.valueOf(zielmengeShots));
                zielmengeBasisView.setText(String.valueOf(zielmengeBasis));
                zielmengeAromaView.setText(String.valueOf(zielmengeAroma));
            }else{
                Toast.makeText(getApplicationContext(), "Berechnung macht keinen Sinn :-)", Toast.LENGTH_SHORT).show();
            }

        }catch (Exception e){
           // Toast.makeText(getApplicationContext(), "Bitte alle Felder füllen!", Toast.LENGTH_SHORT).show();
        }
    }


    public void liquidsMischen(){
        try {

            // Deklaration
            // Basis 1
            EditText basis1MengeInputView = (EditText) findViewById(R.id.basis1MengeInput);
            double basis1MengeInput = Double.parseDouble(basis1MengeInputView.getText().toString());
            EditText basis1StaerkeInputView = (EditText) findViewById(R.id.basis1StaerkeInput);
            if(basis1StaerkeInputView.getText().toString().equals("")){
                basis1StaerkeInputView.setText("0");
            }
            double basis1StaerkeInput = Double.parseDouble(basis1StaerkeInputView.getText().toString());

            // Basis 2
            EditText basis2MengeInputView = (EditText) findViewById(R.id.basis2MengeInput);
            double basis2MengeInput = Double.parseDouble(basis2MengeInputView.getText().toString());
            EditText basis2StaerkeInputView = (EditText) findViewById(R.id.basis2StaerkeInput);
            if(basis2StaerkeInputView.getText().toString().equals("")){
                basis2StaerkeInputView.setText("0");
            }
            double basis2StaerkeInput = Double.parseDouble(basis2StaerkeInputView.getText().toString());

            // Ergebnis
            TextView gesamtMengeView = (TextView) findViewById(R.id.gesamtMenge);
            TextView gesamtStaerkeView = (TextView) findViewById(R.id.gesamtStaerke);


            // Berechnung
            double gesamtMenge = basis1MengeInput+basis2MengeInput;
            gesamtMenge = (double)Math.round(gesamtMenge * 100d) / 100d;
            double gesamtStaerke = ((basis1MengeInput*basis1StaerkeInput)+(basis2MengeInput*basis2StaerkeInput))/(basis1MengeInput+basis2MengeInput);
            gesamtStaerke = (double)Math.round(gesamtStaerke * 100d) / 100d;
            // Setze Ergebnisse
            gesamtMengeView.setText(String.valueOf(gesamtMenge));
            gesamtStaerkeView.setText(String.valueOf(gesamtStaerke));


        }catch (Exception e){
            //Toast.makeText(getApplicationContext(), "Bitte alle Felder füllen!", Toast.LENGTH_SHORT).show();
        }
    }


    public void verduennen(){

        try {
            // Deklaration
            // Akuelle Stärke
            EditText basisAktuelleStaerkeInputView = (EditText) findViewById(R.id.basisAktuellStaerke);
            double basisAktuelleStaerkeInput = Double.parseDouble(basisAktuelleStaerkeInputView.getText().toString());
            if(basisAktuelleStaerkeInputView.getText().toString().equals("")){
                basisAktuelleStaerkeInputView.setText("0");
            }

            // Zielmenge
            EditText zielMengeInputView = (EditText) findViewById(R.id.basisZielmenge);
            double zielMengeInput = Double.parseDouble(zielMengeInputView.getText().toString());

            // Zielstärke
            EditText zielStaerkeInputView = (EditText) findViewById(R.id.zielstaerkeInput);
            double zielStaerkeInput = Double.parseDouble(zielStaerkeInputView.getText().toString());

            // Ergebnis
            TextView benoetigtAktuelleBasisView = (TextView) findViewById(R.id.benoetigtAktuelleBasis);
            TextView benoetigt0erBasisView = (TextView) findViewById(R.id.benoetigt0erBasis);


            // Berechnung
            double aktuelleBasis = zielMengeInput/basisAktuelleStaerkeInput*zielStaerkeInput;
            aktuelleBasis = (double)Math.round(aktuelleBasis * 100d) / 100d;
            double basis = zielMengeInput-aktuelleBasis;
            basis = (double)Math.round(basis * 100d) / 100d;

            // Setze Ergebnisse
            benoetigtAktuelleBasisView.setText(String.valueOf(aktuelleBasis));
            benoetigt0erBasisView.setText(String.valueOf(basis));


        }catch (Exception e){
            //Toast.makeText(getApplicationContext(), "Bitte alle Felder füllen!", Toast.LENGTH_SHORT).show();
        }
    }

    public void aromarechner(){
        try {

            // Deklaration
            // Akuelle Menge Basis
            EditText aromaBasisMengeInputView = (EditText) findViewById(R.id.aroma_BasisMengeInput);
            double aromaBasisMengeInput = Double.parseDouble(aromaBasisMengeInputView.getText().toString());

            // Aktuelle Aromastärke
            EditText aromaAromaMengeInputView = (EditText) findViewById(R.id.aroma_AromaMengeInput);
            double aromaAromaMengeInput = Double.parseDouble(aromaAromaMengeInputView.getText().toString());
            if(aromaAromaMengeInputView.getText().toString().equals("")){
                aromaAromaMengeInputView.setText("0");
            }

            // Zielstärke
            EditText aromaAromaZielStaerkeView = (EditText) findViewById(R.id.aroma_AromaZielStaerkeInput);
            double aromaAromaZielStaerke = Double.parseDouble(aromaAromaZielStaerkeView.getText().toString());
            if(aromaAromaZielStaerkeView.getText().toString().equals("")){
                aromaAromaZielStaerkeView.setText("0");
            }

            // Ergebnis
            TextView aromaBenoetigtesAromaView = (TextView) findViewById(R.id.aroma_BenoetigtesAroma);


            // Berechnung
            double aktuellesAroma = aromaBasisMengeInput*aromaAromaMengeInput/100;
            double zielAroma = aromaBasisMengeInput*aromaAromaZielStaerke/100;

            double addAroma = zielAroma-aktuellesAroma;
            addAroma = (double)Math.round(addAroma * 100d) / 100d;


            // Setze Ergebnisse
            aromaBenoetigtesAromaView.setText(String.valueOf(addAroma));


        }catch (Exception e){
            //Toast.makeText(getApplicationContext(), "Bitte alle Felder füllen!", Toast.LENGTH_SHORT).show();
        }
    }

    public void nikotinerhoehen(){
        try {

            // Deklaration
            // Akuelle Menge Basis
            EditText nikotinBasisMengeInputView = (EditText) findViewById(R.id.nikotin_BasisMengeInput);
            double nikotinBasisMengeInput = Double.parseDouble(nikotinBasisMengeInputView.getText().toString());

            // Aktuelle Nikotinstärke
            EditText nikotinIstStaerkeInputView = (EditText) findViewById(R.id.nikotin_IstStaerkeInput);
            double nikotinIstStaerkeInput = Double.parseDouble(nikotinIstStaerkeInputView.getText().toString());
            if(nikotinIstStaerkeInputView.getText().toString().equals("")){
                nikotinIstStaerkeInputView.setText("0");
            }

            // Shotnikotinstärke
            EditText nikotinShotStaerkeInputView = (EditText) findViewById(R.id.nikotin_ShotStaerkeInput);
            double nikotinShotStaerkeInput = Double.parseDouble(nikotinShotStaerkeInputView.getText().toString());

            // Zielstärke
            EditText nikotinZielStaerkeInputView = (EditText) findViewById(R.id.nikotin_ZielStaerkeInput);
            double nikotinZielStaerkeInput = Double.parseDouble(nikotinZielStaerkeInputView.getText().toString());

            // Ergebnis
            TextView nikotinBenoetigteMengeShotsView = (TextView) findViewById(R.id.nikotin_benoetigteMengeShots);


            // Berechnung

            double addShots = ((nikotinZielStaerkeInput*nikotinBasisMengeInput)-(nikotinBasisMengeInput*nikotinIstStaerkeInput))/(nikotinShotStaerkeInput-nikotinZielStaerkeInput);
            addShots = (double)Math.round(addShots * 100d) / 100d;


            // Setze Ergebnisse
            nikotinBenoetigteMengeShotsView.setText(String.valueOf(addShots));


        }catch (Exception e){
            //Toast.makeText(getApplicationContext(), "Bitte alle Felder füllen!", Toast.LENGTH_SHORT).show();
        }
    }

    public void shakeandvape(){

        try {

            // Deklaration
            // Akuelle Menge Basis
            EditText snvBasisMengeInputView = (EditText) findViewById(R.id.snv_BasisMengeInput);
            double snvBasisMengeInput = Double.parseDouble(snvBasisMengeInputView.getText().toString());

            // Shotnikotinstärke
            EditText snvShotStaerkeInputView = (EditText) findViewById(R.id.snv_ShotStaerkeInput);
            double snvShotStaerkeInput = Double.parseDouble(snvShotStaerkeInputView.getText().toString());

            // Zielstärke
            EditText snvZielStaerkeInputView = (EditText) findViewById(R.id.snv_ZielStaerkeInput);
            double snvZielStaerkeInput = Double.parseDouble(snvZielStaerkeInputView.getText().toString());

            // Ergebnis
            TextView gesamtMengeView = (TextView) findViewById(R.id.snv_gesamtMenge);
            TextView benoetigteMengeShotsView = (TextView) findViewById(R.id.snv_benoetigteMengeShots);


            // Berechnung
            double gesamtMenge = (-snvBasisMengeInput)/((snvZielStaerkeInput/snvShotStaerkeInput)-1);
            gesamtMenge = (double)Math.round(gesamtMenge * 100d) / 100d;
            double benoetigteMengeShots = gesamtMenge-snvBasisMengeInput;
            benoetigteMengeShots = (double)Math.round(benoetigteMengeShots * 100d) / 100d;


            // Setze Ergebnisse
            benoetigteMengeShotsView.setText(String.valueOf(benoetigteMengeShots));
            gesamtMengeView.setText(String.valueOf(gesamtMenge));


        }catch (Exception e){
            //Toast.makeText(getApplicationContext(), "Bitte alle Felder füllen!", Toast.LENGTH_SHORT).show();
        }
    }

}
